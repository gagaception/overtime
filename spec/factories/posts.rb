FactoryGirl.define do
  factory :post do
    date Date.today
    rationale "Some Rationale"
    user
    overtime_request 2.4
  end

  factory :second_post, class: "Post" do
    date Date.yesterday
    rationale "Some more content"
    user
    overtime_request 4.5
  end

  factory :post_from_another_user, class: "Post" do
    date Date.yesterday
    rationale "Some more content"
    non_authorized_user
    overtime_request 3.4
  end
end
