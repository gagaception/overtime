# Overtime app

## Model
- Post -> date:date rationale:text
- User -> Deveise
- AdminUser -> STI

## Features
- Approval Workflow
- SMS Sending -> link to approval or overtime input
- Administrate admin dashboard
- Email summary to managers for approval
- Needs to be documented if employee didn't overtime

## Refactor TODOS:
- User Association integration test in post_spec.rb

## Fix TODOS:
- tests in features/post_spec.rb
