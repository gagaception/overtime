namespace :notification do
  desc "Send sms"
  task sms: :environment do
  # Schedule to run at Sunday at 5 pm
  # Iterate over employees
  # Skip AdminUsers
  # Send a message that has instruction and a link to log time
  # User.all.each do |user|
  #   SmsTool.send_sms()
  end
  desc "Send email notification to manager each day to inform pending overtime request"
  task manager_email: :environment do
    submitted_posts = Post.submitted
    admin_users = AdminUser.all
    if submitted_posts.count > 0
      admin_users.each do |admin|
        ManagerMailer.email(admin).deliver_now
      end
    end
  end
end
